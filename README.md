# OpenFrameworksを用いたゲーム制作キット
## 概要
openFrameworksを使用したゲーム制作を簡単に行えるように各種クラスを整理したゲーム制作キットです。今後様々な制作ケースに対応する予定です。

## 使い方
次のものを用意してください。
- [openFrameworks(ver0.11.2で制作)](https://openframeworks.cc/)
- mainブランチをダウンロードしたデータ
- Xcode
<br>※本データはMacOS向けに制作したものですが、Windowsでの制作にも利用可能です。ここではMacOS向けの操作をまとめています。
1. mainブランチをダウンロードしたデータを解凍しておきます。
2. 解凍して生成されたフォルダ(gamedevelopmentkit-main)の中にXcodeのプロジェクトがあることを確認します(これがopenFrameworksのプロジェクトデータになります)。
3. openFrameworksのProject Generatorを起動し、create/updateのProject pathを上記のフォルダに設定します。
4. Updateを行います。成功すると、Xcodeが起動し、制作を開始できます。