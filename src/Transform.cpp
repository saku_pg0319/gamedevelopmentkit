//
//  Transform.cpp
//  Final
//
//  Created by hw20a029 on 2022/12/02.
//

#include "Transform.hpp"

void Transform::SetTransform(vec2 pos, float rotate, vec2 objSize){
    position = pos;
    rotation = rotate;
    size = objSize;
}

void Transform::Translate(float x, float y){
    position+=vec2(x, y);
}

void Transform::Rotate(float angle){
    rotation+=angle;
    while(rotation >= 360){
        rotation-=360;
    }
    
}

void Transform::SetPosition(float x, float y){
    position = vec2(x, y);
}

void Transform::SetRotation(float angle){
    rotation = angle;
}
