//
//  PhysicalActor.cpp
//  Final
//
//  Created by hw20a029 on 2022/12/09.
//

#include "PhysicalActor.hpp"

PhysicalActor::PhysicalActor() : Actor()
{
    physics.velocity = vec2(0, 0);
}

void PhysicalActor::SetUp()
{
}

void PhysicalActor::Update()
{
    if (physics.useGravity)
    {
        physics.velocity += vec2(0, physics.gravityScale);
    }
    transform.position += physics.velocity;
}
void PhysicalActor::Draw()
{
}
void PhysicalActor::KeyPressed(int keycode)
{
}
void PhysicalActor::KeyReleased(int keycode)
{
}
void PhysicalActor::MouseMoved(int x, int y)
{
}
void PhysicalActor::MousePressed(int x, int y, int button)
{
}
void PhysicalActor::MouseReleased(int x, int y, int button)
{
}
void PhysicalActor::MouseEntered(int x, int y)
{
}
void PhysicalActor::MouseExited(int x, int y)
{
}
