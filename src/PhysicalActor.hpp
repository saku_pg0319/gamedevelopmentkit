//
//  PhysicalActor.hpp
//  Final
//
//  Created by hw20a029 on 2022/12/09.
//

#ifndef PhysicalActor_hpp
#define PhysicalActor_hpp

#include "Actor.hpp"
#include "Physics.hpp"

class PhysicalActor : public Actor{
protected:

public:
    PhysicalActor();
    Physics physics;

    virtual void SetUp() override;
    virtual void Update() override;
    virtual void Draw() override;
    virtual void KeyPressed(int keycode) override;
    virtual void KeyReleased(int keycode) override;
    virtual void MouseMoved(int x, int y) override;
    virtual void MousePressed(int x, int y, int button) override;
    virtual void MouseReleased(int x, int y, int button) override;
    virtual void MouseEntered(int x, int y) override;
    virtual void MouseExited(int x, int y) override;
};

#endif /* PhysicalActor_hpp */
