#include "ofApp.h"
#include <vector>
#include "Scene.hpp"
/// <Scenes>
/// </Scenes>
///

static int currentLoadSceneNum = 0;
Scene *currentLoadScene = nullptr;

vector<Scene *> scenes = {};

string currentLoadFont;
float currentLoadFontSize;

///Load scene without destroying
void LoadScene(int nextSceneNum){
    currentLoadSceneNum = nextSceneNum;
    currentLoadScene = scenes[currentLoadSceneNum];
}

///Load scene and destroy previous scene
void LoadSceneWithDestroy(int nextSceneNum){
    cout<<"Destroy Loading"<<endl;
    int tmp = currentLoadSceneNum;
    currentLoadSceneNum = nextSceneNum;
    scenes[tmp] = nullptr;
    switch(tmp){
        default:
            break;
            
    }
    currentLoadScene = scenes[currentLoadSceneNum];
}

///Reload current scene with destroying
void ReloadScene(){
    LoadSceneWithDestroy(currentLoadSceneNum);
}

/// Get Adress of CurrentLoadScene
intptr_t GetCurrentLoadScene(){
    intptr_t scenePtr = (intptr_t)currentLoadScene;
    return scenePtr;
}

intptr_t GetOtherActor(string objName){
    Scene *currentScene = (Scene *)GetCurrentLoadScene();
    for(auto obj : currentScene->GetActorList()){
        string s = obj->GetName();
        if(objName == s){
            return (intptr_t)obj;
        }
    }
    return (intptr_t)nullptr;
}

intptr_t GetOtherGameObj(string objName){
    Scene *currentScene = (Scene *)GetCurrentLoadScene();
    for(auto obj : currentScene->GetGameObjList()){
        string s = obj->GetName();
        if(objName == s){
            return (intptr_t)obj;
        }
    }
    return (intptr_t)nullptr;
}

intptr_t GetOtherText(string objName){
    Scene *currentScene = (Scene *)GetCurrentLoadScene();
    for(auto obj : currentScene->GetTextList()){
        string s = obj->GetName();
        if(objName == s){
            return (intptr_t)obj;
        }
    }
    return (intptr_t)nullptr;
}

//--------------------------------------------------------------
void ofApp::setup(){
    ofSetDataPathRoot("../Resources/");
    ofSetVerticalSync(true);
    ofSetFrameRate(60);
//    for(int i = 0; i < 10; i++){
//        playerScoreList.push_back(PlayerScore("---", 0));
//    }
    currentLoadScene = scenes[currentLoadSceneNum];
    currentLoadScene->SetUp();
}

//--------------------------------------------------------------
void ofApp::update(){
    bool bw = ofGetWidth() != windowSizeWidth;
    bool bh = ofGetHeight() != windowSizeHeight;
    if(bw || bh){
        ofSetWindowShape(windowSizeWidth, windowSizeHeight);
    }
    currentLoadScene->Update();
}

//--------------------------------------------------------------

void ofApp::draw(){
    currentLoadScene->Draw();
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
    currentLoadScene->KeyPressed(key);
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){
    
}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){

}
