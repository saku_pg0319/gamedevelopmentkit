//
//  Sprite.hpp
//  Class15
//
//  Created by hw20a029 on 2022/12/09.
//

#ifndef Sprite_hpp
#define Sprite_hpp

#include <stdio.h>
#include <iostream>
#include "ofApp.h"
#include "SpriteShape.hpp"

using namespace std;
using namespace glm;



//Sprite for Object
class Sprite{
protected:
    bool isEnableImage;
    int shape;
    vec4 color;//(r, g, b, a)
    
public:
    bool useFillColor;
    Sprite(bool useImage, int shapeNum, vec4 colorVector);
    void SetColor(int r, int g, int b, int a);
    void DrawVisual(int x, int y, float sizeX, float sizeY);
    void SetSize(float x, float y);
};


#endif /* Sprite_hpp */
