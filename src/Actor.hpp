//
//  Actor.hpp
//  Final
//
//  Created by hw20a029 on 2022/12/07.
//

#ifndef Actor_hpp
#define Actor_hpp

#include "GameObject.hpp"
#include <iostream>
using namespace std;

//Class with updating per frame

class Actor : public GameObject{
protected:
    bool hasCollision = false;
public:
    Actor();
    virtual void SetUp(); //Called first frame
    virtual void Update(); //Called per frames
    virtual void Draw(); //Called after Update
    virtual void KeyPressed(int keycode); //Called moment that some key was pressed
    virtual void KeyReleased(int keycode); //Called moment that some key was released
    virtual void MouseMoved(int x, int y); //Called during mouse being moved
    virtual void MousePressed(int x, int y, int button); //Called moment that some mouse button was pressed
    virtual void MouseReleased(int x, int y, int button); //Called moment that some mouse button was released
    virtual void MouseEntered(int x, int y); //Called moment that mouse cursur entering to window area
    virtual void MouseExited(int x, int y); //Called moment that mouse cursur exiting to window area
    
};

#endif /* Actor_hpp */
