//
//  CollisionActor.cpp
//  Class15
//
//  Created by hw20a029 on 2022/12/13.
//

#include "CollisionActor.hpp"

CollisionActor::CollisionActor(){
    
}

void CollisionActor::SetUp(){
}
void CollisionActor::Update(){
}
void CollisionActor::Draw(){
}
void CollisionActor::KeyPressed(int keycode){
}
void CollisionActor::KeyReleased(int keycode){
}
void CollisionActor::MouseMoved(int x, int y){
}
void CollisionActor::MousePressed(int x, int y, int button){
}
void CollisionActor::MouseReleased(int x, int y, int button){
}
void CollisionActor::MouseEntered(int x, int y){
}
void CollisionActor::MouseExited(int x, int y){
}
