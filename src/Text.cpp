//
//  Text.cpp
//  Class15
//
//  Created by hw20a029 on 2022/12/16.
//

#include "Text.hpp"

Text::Text(){
    fontSetting = nullptr;
}

void Text::SetUp(){
}
void Text::Update(){
}
void Text::Draw(){
    ofSetColor(color.r, color.g, color.b, color.a);
    f.load(*fontSetting);
    f.drawString(text, transform.position.x, transform.position.y);
}
void Text::KeyPressed(int keycode){
}
void Text::KeyReleased(int keycode){
}
void Text::MouseMoved(int x, int y){
}
void Text::MousePressed(int x, int y, int button){
}
void Text::MouseReleased(int x, int y, int button){
}
void Text::MouseEntered(int x, int y){
}
void Text::MouseExited(int x, int y){
}

void Text::SetFont(string _fontName, float _fontSize){
    fontSetting = new ofTrueTypeFontSettings(_fontName, _fontSize);
    //cout<<"Font: "<<fontSetting->fontName<<endl;
    
}

string Text::GetName(){
    return name;
}

float Text::GetTextWidth(){
    f.load(*fontSetting);
    return f.stringWidth(text);
}

float Text::GetTextHeight(){
    f.load(*fontSetting);
    return f.stringHeight(text);
}
