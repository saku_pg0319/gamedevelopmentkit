//
//  Scene.cpp
//  Final
//
//  Created by hw20a029 on 2022/12/02.
//

#include "Scene.hpp"

Scene::Scene(){
    gameObjects.clear();
    actors.clear();
}

void Scene::SetUp(){
    isLoadFirstFrame = true;
    for(auto obj : actors){
        obj->SetUp();
    }
    for(auto obj : texts){
        obj->SetUp();
    }
    for(auto obj : gameObjects){
        obj->SetUp();
    }
}
void Scene::Update(){
    if(!isLoadFirstFrame){
        SetUp();
        isLoadFirstFrame = true;
    }
    for(auto obj : actors){
        obj->Update();
    }
    for(auto obj : texts){
        obj->Update();
    }
}
void Scene::Draw(){
    ofBackground(0, 0, 0);
    for(auto obj : actors){
        obj->Draw();
    }
    for(auto obj : texts){
        obj->Draw();
    }
    for(auto obj : gameObjects){
        obj->Draw();
    }
}
void Scene::KeyPressed(int keycode){
    for(auto obj : actors){
        obj->KeyPressed(keycode);
    }
    for(auto obj : texts){
        obj->KeyPressed(keycode);
    }
}
void Scene::KeyReleased(int keycode){
    for(auto obj : actors){
        obj->KeyReleased(keycode);
    }
    for(auto obj : texts){
        obj->KeyReleased(keycode);
    }
}
void Scene::MouseMoved(int x, int y){
    for(auto obj : actors){
        obj->MouseMoved(x, y);
    }
    for(auto obj : texts){
        obj->MouseMoved(x, y);
    }
}
void Scene::MousePressed(int x, int y, int button){
    for(auto obj : actors){
        obj->MousePressed(x, y, button);
    }
    for(auto obj : texts){
        obj->MousePressed(x, y, button);
    }
}
void Scene::MouseReleased(int x, int y, int button){
    for(auto obj : actors){
        obj->MouseReleased(x, y, button);
    }
    for(auto obj : texts){
        obj->MouseReleased(x, y, button);
    }
}
void Scene::MouseEntered(int x, int y){
    for(auto obj : actors){
        obj->MouseEntered(x, y);
    }
    for(auto obj : texts){
        obj->MouseEntered(x, y);
    }
}
void Scene::MouseExited(int x, int y){
    for(auto obj : actors){
        obj->MouseExited(x, y);
    }
    for(auto obj : texts){
        obj->MouseExited(x, y);
    }
}

string Scene::GetName(){
    return name;
}

void Scene::AddActor(Actor *actor){
    actors.push_back(actor);
    actor->SetUp();
}

void Scene::AddGameObj(GameObject *gameObject){
    gameObjects.push_back(gameObject);
}

vector<Actor *> Scene::GetActorList(){
    return actors;
}
vector<GameObject *> Scene::GetGameObjList(){
    return gameObjects;
}
vector<Text *> Scene::GetTextList(){
    return texts;
}
