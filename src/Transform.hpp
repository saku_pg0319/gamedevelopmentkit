//
//  Transform.hpp
//  Class15
//
//  Created by hw20a029 on 2022/12/09.
//

#ifndef Transform_hpp
#define Transform_hpp

#include <stdio.h>
#include <iostream>
#include "ofApp.h"
using namespace std;
using namespace glm;

//Transform Component
struct Transform{
public:
    vec2 position;
    float rotation;
    vec2 size;
    
    void SetTransform(vec2 pos, float rotate, vec2 objSize);
    void Translate(float x, float y);
    void Rotate(float angle);
    void SetPosition(float x, float y);
    void SetRotation(float angle);
};

#endif /* Transform_hpp */
