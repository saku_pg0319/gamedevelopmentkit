//
//  Physics.hpp
//  Class15
//
//  Created by hw20a029 on 2022/12/09.
//

#ifndef Physics_hpp
#define Physics_hpp

#include "ofApp.h"
#include <iostream>

using namespace std;
using namespace glm;

//Physical Option
struct Physics{
    float gravityScale = 0.01f;
    float bounciness = 0.5f;
    float staticFriction = 0.5f;
    float dynamicFriction = 0.5f;
    bool useGravity = true;
    bool isKinematic = false;
    vec2 velocity;
};

#endif /* Physics_hpp */
