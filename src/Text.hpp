//
//  Text.hpp
//  Class15
//
//  Created by hw20a029 on 2022/12/16.
//

#ifndef Text_hpp
#define Text_hpp

#include <iostream>
#include "ofApp.h"
#include "Transform.hpp"
#include <string>

using namespace std;

class Text{
public:
    ofTrueTypeFont f;
    ofTrueTypeFontSettings *fontSetting;
    Transform transform;
    string text;
    vec4 color; //(r, g, b, a)
    Text();
    void SetFont(string _fontName, float _fontSize);
    virtual void SetUp(); //Called first frame
    virtual void Update(); //Called per frames
    virtual void Draw(); //Called after Update
    virtual void KeyPressed(int keycode); //Called moment that some key was pressed
    virtual void KeyReleased(int keycode); //Called moment that some key was released
    virtual void MouseMoved(int x, int y); //Called during mouse being moved
    virtual void MousePressed(int x, int y, int button); //Called moment that some mouse button was pressed
    virtual void MouseReleased(int x, int y, int button); //Called moment that some mouse button was released
    virtual void MouseEntered(int x, int y); //Called moment that mouse cursur entering to window area
    virtual void MouseExited(int x, int y); //Called moment that mouse cursur exiting to window area
    float GetTextWidth();
    float GetTextHeight();
    string GetName();
protected:
    string name;
    
};


#endif /* Text_hpp */
