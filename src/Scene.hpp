//
//  Scene.hpp
//  Class15
//
//  Created by hw20a029 on 2022/12/09.
//

#ifndef Scene_hpp
#define Scene_hpp

#include <iostream>
#include <vector>
#include "GameObject.hpp"
#include "Actor.hpp"
#include "Text.hpp"

using namespace std;

class Scene{
protected:
    vector<GameObject *> gameObjects;
    vector<Actor *> actors;
    vector<Text *> texts;
    
    bool isLoadFirstFrame = false;
    string name;
public:
    Scene(); //Set Object List
    virtual void SetUp(); //Called first frame
    virtual void Update(); //Called per frames
    virtual void Draw(); //Called after Update()
    virtual void KeyPressed(int keycode); //Called moment that some key was pressed
    virtual void KeyReleased(int keycode); //Called moment that some key was released
    virtual void MouseMoved(int x, int y); //Called during mouse being moved
    virtual void MousePressed(int x, int y, int button); //Called moment that some mouse button was pressed
    virtual void MouseReleased(int x, int y, int button); //Called moment that some mouse button was released
    virtual void MouseEntered(int x, int y); //Called moment that mouse cursur entering to window area
    virtual void MouseExited(int x, int y); //Called moment that mouse cursur exiting to window area
    
    vector<Actor *> GetActorList();
    vector<GameObject *> GetGameObjList();
    vector<Text *> GetTextList();
    
    void AddActor(Actor *actor);
    void AddGameObj(GameObject *gameObject);
    string GetName();
};

#endif /* Scene_hpp */
