//
//  Sprite.cpp
//  Class15
//
//  Created by hw20a029 on 2022/12/09.
//

#include "Sprite.hpp"

Sprite::Sprite(bool useImage, int shapeNum, vec4 colorVector){
    shape = shapeNum;
    SetColor(colorVector.r, colorVector.g, colorVector.b, colorVector.a);
}


void Sprite::SetColor(int r, int g, int b, int a){
    color = vec4(r, g, b, a);
}

void Sprite::DrawVisual(int x, int y, float sizeX, float sizeY){
    if(useFillColor){
        ofFill();
    }else{
        ofNoFill();
    }
    ofSetColor(color.r, color.g, color.b, color.a);
    //cout<<"Color: R"<<color.r<<", G"<<color.g<<", B"<<color.b<<"Alpha->"<<color.a<<endl;
    switch(shape){
        case noneshape:
            break;
        case circle:
            ofDrawCircle(x, y, (sizeX+sizeY)/2);
            //cout<<"Circle"<<endl;
            break;
        case square:
            ofDrawRectangle(x-sizeX/2, y-sizeY/2, x+sizeX, y+sizeY);
            break;
        
    }
}

