//
//  Actor.cpp
//  Class15
//
//  Created by hw20a029 on 2022/12/09.
//

#include "Actor.hpp"

Actor::Actor() : GameObject(){
    
}

void Actor::SetUp(){
}
void Actor::Update(){
}
void Actor::Draw(){
}
void Actor::KeyPressed(int keycode){
}
void Actor::KeyReleased(int keycode){
}
void Actor::MouseMoved(int x, int y){
}
void Actor::MousePressed(int x, int y, int button){
}
void Actor::MouseReleased(int x, int y, int button){
}
void Actor::MouseEntered(int x, int y){
}
void Actor::MouseExited(int x, int y){
}
